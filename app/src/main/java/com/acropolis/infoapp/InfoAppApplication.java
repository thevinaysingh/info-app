package com.acropolis.infoapp;

import android.app.Application;
import android.content.ContextWrapper;
import android.util.Log;

import java.io.IOException;


public class InfoAppApplication extends Application {
    public static final String TAG = InfoAppApplication.class.getSimpleName();
    public static InfoAppApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();

        new PreferencesManagerUtils.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

    }

    private void initApplication(){
        instance = this;
    }
}