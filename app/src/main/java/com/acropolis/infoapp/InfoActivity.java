package com.acropolis.infoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoActivity extends AppCompatActivity {


    @BindView(R.id.info_title)
    TextView info_title;

    public static final String EXTRA_BUTTON_ID = "buttonId";
    private ButtonSelectedEnum buttonSelectedEnum = ButtonSelectedEnum.AIRPORT;

    public static void startForResult(Activity activity, ButtonSelectedEnum buttonSelectedEnum) {
        Intent intent = new Intent(activity, InfoActivity.class);
        intent.putExtra(InfoActivity.EXTRA_BUTTON_ID, buttonSelectedEnum);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_info);
        ButterKnife.bind(this);

        parseIntentExtras();

        initUI();
    }

    private void initUI() {
        switch (buttonSelectedEnum) {
            case INDORE:
                info_title.setText("You have asked details for indore");
                break;
            case AIRPORT:
                info_title.setText("You have asked details for airport");
                break;
            case INDORE_FLIGHT:
                info_title.setText("You have asked details for flights to or from indore");
                break;
            default:
                // do nothing
                break;
        }
    }


    private void parseIntentExtras() {
        buttonSelectedEnum = (ButtonSelectedEnum)getIntent().getExtras().getSerializable(InfoActivity.EXTRA_BUTTON_ID);
    }
}
