package com.acropolis.infoapp;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class BluetoothConnectionService extends Service {

    String devicePrefix = "HC-05";
    private boolean deviceConnected = false;
    private BluetoothDevice device = null;
    private BluetoothConnectionThread mBluetoothConnectionThread = null;
    private ConnectedThread mConnectedThread = null;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;

    public BluetoothConnectionService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public BluetoothConnectionService() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if(btAdapter == null) {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        if(mBluetoothConnectionThread == null) {
            mBluetoothConnectionThread = new BluetoothConnectionThread();
            mBluetoothConnectionThread.start();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        deviceConnected = false;
        Intent broadcastIntent = new Intent(this, SensorRestarterBroadcastReceiver.class);
        sendBroadcast(broadcastIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private BluetoothDevice getValidBluetoothDevice() {
        final List<BluetoothDevice> pairedDevices = new ArrayList<BluetoothDevice>(btAdapter.getBondedDevices());
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().toUpperCase().startsWith(devicePrefix)){
                    return device;
                }

                // fixme: compare using devie nae
                if (device.getAddress().equals("20:14:02:17:17:79")){
                    return device;
                }
            }
        }
        return null;
    }

    private class BluetoothConnectionThread extends Thread {

        public BluetoothConnectionThread() { }

        @Override
        public void run() {
            super.run();
            while (true) {
                if(btAdapter != null && btAdapter.isEnabled()) {
                    device = getValidBluetoothDevice();
                    if(device != null && !deviceConnected) {
                        try {
                            if(btSocket == null) {
                                btSocket = createBluetoothSocket(device);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Establish the Bluetooth socket connection.
                        try
                        {
                            if(!btSocket.isConnected()) {
                                btSocket.connect();
                            }
                        } catch (IOException e) {
                            try
                            {
                                btSocket.close();
                            } catch (IOException e2)
                            {
                                e.printStackTrace();
                            }
                        }

                        if(mConnectedThread == null) {
                            mConnectedThread = new ConnectedThread(btSocket);
                            mConnectedThread.start();
                        }

                    } else {
                        if(mConnectedThread != null) {
                            mConnectedThread = null;
                        }
                        if(btSocket != null) {
                            btSocket = null;
                        }
                    }
                } else {
                    if(mConnectedThread != null) {
                        mConnectedThread = null;
                    }
                    if(btSocket != null) {
                        btSocket = null;
                    }
                    if(deviceConnected) {
                        deviceConnected = false;
                    }
                }
            }
        }

    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return  device.createRfcommSocketToServiceRecord(device.getUuids()[0].getUuid());
    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    deviceConnected = true;
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    if(readMessage.length() > 30 && !PreferencesManagerUtils.getBoolean(Constants.IS_APPLICATION_OPENED, false)) {
                        PreferencesManagerUtils.putBoolean(Constants.IS_APPLICATION_OPENED, true);
                        Intent dialogIntent = new Intent(BluetoothConnectionService.this, SplashActivity.class);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }
    }
}